"""Configure test fixtures."""
import pytest

from testflask import create_app


@pytest.fixture
def client():
    """The client fixture provides a simple interface to the application."""
    app = create_app({'TESTING': True})
    with app.test_client() as client:
        yield client
