def test_hello(client):
    """Test whether the application says hello."""
    rv = client.get('/')
    assert 'hello' in str(rv.data).casefold()
